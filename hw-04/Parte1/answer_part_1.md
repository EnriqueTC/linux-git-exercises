Ejercicio 1:

	Para el ejercicio 1 usaremos el siguiente comando para generar la lista deseada. (Adjunto nginx_requests_total.txt)	

	grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' nginx_logs_examples.log | sort -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4 | uniq -cd | awk '{print $2 " -> " $1}' > nginx_requests_total.txt

Ejercicio 2:

	Para realizar este ejercicio se ha usado el script backup-script.sh adjuntado en la carpeta de este ejercicio.

Ejercicio 3:

	El primer paso es dar permisos para que cron pueda ejecutar elk script: chmod +x /tmp/backup-script.sh
	A continuación instalamos corn con el comando apt-get install cron.
	Luego editamos el fichero de cron con el comando crontab -e.
	Añadimos la siguiente linia: (Adjunto img1.png)
		59 23 * * * /tmp/backup-script.sh enrique

	Como especifica el enunciada el script se ejecutará de lunes a domingo a las 23:59 durante todos los meses del año.

NOTA: tanto el primer archivo como el segundo los he ejecutado dentro de la carpeta /tmp de linux.
NOTA: en el script para realizar los backups el nombre de usuario se pasa por parametro al ejecutar el script.
	
	