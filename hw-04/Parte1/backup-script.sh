#!/bin/bash

date=$(date)
day=$(date +%d)
month=$(date +%m)
year=$(date +%Y)
week_day=$(date +%u)

generic_path=/backup/$1
path=$generic_path/$year/$month/$day
generic_file=nginx_log_requests_
file=$generic_file$year$month$day.log

folder_tar=nginx_logs_$year$month$day

if [ -d $path ]
then
	cp /tmp/nginx_requests_total.txt /tmp/$file
	mv /tmp/$file $path
else	
	mkdir -p $path
	cp /tmp/nginx_requests_total.txt /tmp/$file
	mv /tmp/$file $path
fi

if [ $week_day == 7 ]
then
	y=7
	i=0
	
	mkdir -p $folder_tar
	
	while [ $y -gt 0 ]
	do
		aux_day=$(date -d "$i days ago" +%d)
		aux_month=$(date -d "$i days ago" +%m)
		aux_year=$(date -d "$i days ago" +%Y)

		new_file=$generic_path/$aux_year/$aux_month/$aux_day/$generic_file$aux_year$aux_month$aux_day.log

		if [ -f $new_file  ]
		then
			cp $new_file $folder_tar
		fi

		y=$(( $y - 1 ))
		i=$(( $i + 1 ))
		
	done

	tar -czf $path/nginx_logs_$year$month$day.tar.gz --absolute-names $folder_tar

	rm -rf $folder_tar
fi
