Primero de todo tenemos que configurar el usuario y correo de git:

	git config --global user.name "Enrique Torrents Cuevas"
	git config --global user.email "cucho_tc@hotmail.com"

1- El primer passo es crear el repository en Bitbucket (en mi caso) para 
	
	mkdir part2_git_project && cd part2_git_project
	git init

2- Añadimos la regla para ignorar los ficheros con extensión .sh. De esta manera los ficheros con extensión .sh no se subirán al repositorio de git.

	echo '*.sh' >> .gitignore

3- Creación de los dos ficheros y los añadimos a la stage area.
	
	touch demo1.txt | touch demo2.txt
	git add demo1.txt demo2.txt

4- Añadimos un mensaje al fichero demo1.txt y hacemos un commit.

	echo "Some test message demo1" >> demo1.txt
	git add demo1.txt
	git commit -m "Modified demo1.txt"

5- Añadimos un mensaje al fichero demo2.txt y hacemos un commit.

	echo "Some test message demo2" >> demo2.txt
	git add demo2.txt
	git commit -m "Modified demo3.txt"

6- Rectificamos el commit anterior.

	git commit --amend -m "Modified demo2.txt"

7- Creamos una nueva rama y hacemos checkout de la branch nueva. También creamos el script script.sh y que muestre Git 101 por pantalla.

	git checkout -b develop
	echo "#!/bin/bash\necho Git 101" >> script.sh

8- Damos permisos de ejución al script.sh.

	chmod +x script.sh

9- Para realizar un merge de develop a master, primero tenemos que hacer un commit de todo lo modificado en la rama actual (develop), cambiar a la branch de master (checkout) y ejecutar el merge.

	git add --all
	git commit -m "Last commit"
	git checkout master
	git merge develop

10- Por último subiremos los cambios al repositorio. Primero configuramos el proyecto remoto de git. Posteriormente hacemos push de los cambios.

	git remote add origin https://EnriqueTC@bitbucket.org/EnriqueTC/part2_git_project.git
	git push -u origin master

Extra: 	- la imagen adjunta img1.png muestra el procedimiento y resultado de todos los pasos realizados. 
	- la imagen adjunta img2.png muestra el resultado de la ejecución del commando (git log --oneline > verification.log) y el commit del mismo para subirlo al repository remoto de git. (Adjunto verification.log)
	- por último la imagen adjunta img3.png muestra los archivos creados en el repository local dónde se puede observar el fichero script.sh y la imagen adjunta img4.png muestra como este fichero no está en el repositorio remoto ya que en el punto 2 se ha añadido la condición en el gitignore de no incluir los ficheros con extensión .sh
	- para comprobar el ejercicio dejo los datos del repositorio y el último commit:
		Repository: https://bitbucket.org/EnriqueTC/part2_git_project/src/master/ 
		Commit code: e3e536c